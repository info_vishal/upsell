<div>
    <ul role="tablist" class="Polaris-Tabs Polaris-Tabs--fillSpace hidden">
        <li role="presentation" class="Polaris-Tabs__DisclosureTab Polaris-Tabs__DisclosureTab--visible">
            <div>
                <button tabindex="-1" class="Polaris-Tabs__DisclosureActivator" aria-controls="Popover1" aria-owns="Popover1"
                    aria-haspopup="true" aria-expanded="false">
                    <span class="Polaris-Icon">
                        <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                            <path d="M6 10a2 2 0 1 1-4.001-.001A2 2 0 0 1 6 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 12 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 18 10z"
                                fill-rule="evenodd"></path>
                        </svg>
                    </span>
                </button>
            </div>
        </li>
    </ul>
    <div class="Polaris-Tabs">
        <li role="presentation" class="Polaris-Tabs__TabContainer">
            <a href="{{ route('home') }}">
                <button id="custom_module_nav" role="tab" tabindex="-1" class="Polaris-Tabs__Tab {{ Request::is('/') ? 'Polaris-Tabs__Tab--selected' : '' }}" aria-selected="false">
                    <span class="Polaris-Tabs__Title">Dashboard</span>
                </button>
            <a>
        </li>
        <li role="presentation" class="Polaris-Tabs__TabContainer">
            <a href="{{ route('upsell.index') }}">
                <button id="features_nav" role="tab" tabindex="-1" class="Polaris-Tabs__Tab {{ Request::is('upsells*') ? 'Polaris-Tabs__Tab--selected' : '' }}" aria-selected="false">
                    <span class="Polaris-Tabs__Title">Upsell</span>
                </button>
            </a>
        </li>
        
        <button tabindex="-1" class="Polaris-Tabs__DisclosureActivator hidden">
            <span class="Polaris-Icon">
                <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true">
                    <path d="M6 10a2 2 0 1 1-4.001-.001A2 2 0 0 1 6 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 12 10zm6 0a2 2 0 1 1-4.001-.001A2 2 0 0 1 18 10z"
                        fill-rule="evenodd"></path>
                </svg>
            </span>
        </button>
    </div>
</div>