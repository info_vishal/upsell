@extends('layouts.header')
@section('content')
<div class="Polaris-Page__Content Polaris-Page__Header--hasSeparator">
    <div class="Polaris-Layout">
        <div class="Polaris-Layout__AnnotatedSection">
            <div class="Polaris-Layout__AnnotationWrapper">
                <div class="Polaris-Layout__AnnotationContent">
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Section">
                            <div class="Polaris-CalloutCard">
                                <div class="Polaris-CalloutCard__Content">
                                    <div class="Polaris-Stack Polaris-Stack--alignmentBaseline">
                                        <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                          <h2 class="Polaris-Heading">Upsell Offers</h2>
                                        </div>
                                        <div class="Polaris-Stack__Item">
                                          <div class="Polaris-ButtonGroup">
                                            <div class="Polaris-ButtonGroup__Item Polaris-ButtonGroup__Item--plain"><font class="Polaris-Button Polaris-Button--primary new-field-btn" id="myBtn"><span class="Polaris-Button__Content"><span>Upsell Creator</span></span></font></div>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="Polaris-TextContainer">
                                        <p>
                                            
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection