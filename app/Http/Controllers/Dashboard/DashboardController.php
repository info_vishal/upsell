<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public $view = 'pages.dashboard.';
	
    public function index(){
    	return view($this->view.'index');
    }
}
