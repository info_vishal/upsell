<?php

namespace App\Http\Controllers\Upsell;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpsellController extends Controller
{
	public $view = 'pages.upsell.';

    public function index(){
    	return view($this->view.'index');
    }
}
