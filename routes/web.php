<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web', 'auth.shop']], function () {
    /* App Installation */
    Route::get('/','Dashboard\DashboardController@index')->name('home');
    Route::get('/upsells','Upsell\UpsellController@index')->name('upsell.index');
});

Route::get('flush', function(){
    request()->session()->flush();
});